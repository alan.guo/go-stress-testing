# Go Stress Testing
This project is a stress tester for a concurrent trading system. It simulates multiple traders placing orders concurrently and generates a high volume of trading activity. The stress tester is implemented in Go.

## Usage
To run the stress tester, follow the steps below:
- Build and run the stress tester with the desired configuration using the provided Makefile targets. There are three predefined targets available:
  - orderpersec: Simulates 1 order per second.
  - order15persec: Simulates 15 orders per second.
  - order60persec: Simulates 60 orders per second.
- Choose the appropriate target based on the desired stress level. For example, to simulate 15 orders per second, use the following command:
    ```
    make order15persec
    ```
- Note: The Makefile targets use environment variables to configure the stress tester. You can modify these variables directly in the Makefile or pass them as command-line arguments.
- The stress tester will start running and output the relevant information to the console. The trading activity will be logged to a log file. 
- Wait for the stress test to complete. The test will automatically terminate after a specified timeout period. 
- After the stress test is finished, review the results in the log file and the generated statistics file.

## Configuration
The stress tester can be configured by modifying the variables in the main.go file or by passing them as environment variables.

The following configuration options are available:
- `ORDER_FREQUENCY`: The frequency at which orders are placed (default: 1s).
- `TIME_OUT`: The timeout period for the stress test (default: 4s).
- `NUM_SYMBOLS`: The number of symbols to be traded (default: 15).
- `TRADERID_START`: The starting trader ID (default: 5001).
- `TRADERID_END`: The ending trader ID (default: 5005).

To modify the configuration, locate the corresponding variables in the main.go file and adjust their values as needed.
Alternatively, you can set the environment variables before running the stress tester. For example:
```
ORDER_FREQUENCY=66ms TIME_OUT=80s NUM_SYMBOLS=120 TRADERID_END=5050 go run main.go
```

## Results
The stress tester generates two output files:
- `trading.log`: This file contains the log messages generated during the stress test, including information about the trading activity.
- `trading_stat.log`: This file contains statistical information, such as the order frequency, timeout duration, number of symbols, and trader ID range.

After running the stress test, you can analyze these files to evaluate the performance and behavior of the trading system under stress conditions.
